package pl.poznan.put.spio;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import pl.poznan.put.spio.db.DB;
import pl.poznan.put.spio.service.Catalog;
import pl.poznan.put.spio.service.Person;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CatalogTest {

    @Mock
    private DB dbMock;

    @Test
    public void shouldGetPersonByPesel() {
        // given
        Person p = new Person("imie", "nazwisko", "pesel");
        when(dbMock.getPerson("pesel")).thenReturn(p);// instruujemy zachowanie,
        // dla takiego wywołania, zwróć takie wartości

        Catalog c = new Catalog(dbMock, null);// tworzymy obiekt testowany,
        // spełniając zależność mockiem

        // when
        Person result = c.getPerson("pesel");// właściwy test

        // then
        assertEquals(p, result);// weryfikacja testu
        verify(dbMock, atLeastOnce()).getPerson("pesel");// dodatkowa
        // weryfikacja mocka
    }

}
