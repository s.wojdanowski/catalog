package pl.poznan.put.spio.service;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.poznan.put.spio.db.DB;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

// only for tests
@Configuration
public class CatalogMockInjector {

    @Bean
    public DB db() {
        return new DB() {
            private Map<String, Person> db = new HashMap<>();

            {
                db.put("1111116", Person.builder()
                        .name("A")
                        .surname("B")
                        .pesel("1111116")
                        .build());
            }

            @Override
            public List<String> getPESELList(String name, String surname) {
                return db.values()
                        .stream()
                        .filter(p -> p.getName().equals(name) && p.getSurname().equals(surname))
                        .map(p -> p.getPesel())
                        .collect(Collectors.toList());
            }

            @Override
            public Person getPerson(String pesel) {
                return db.get(pesel);
            }

            @Override
            public void insertPerson(Person person) {
                db.put(person.getPesel(), person);
            }
        };
    }

    @Bean
    public PESELService peselService() {
        return pesel -> true;
    }

}
